<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserPersister implements DataPersisterInterface
{
    protected $em;
    protected $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }
    
    public function supports ($data): bool
    {
        return $data instanceof User;
    }

    public function persist($data)
    {
        // 1. Mettre le status de l'utilisateur offline
        $data->setStatus("offline");


        $data->setPassword($this->encoder->encodePassword($data, $data->getPassword()));

        // 2. Demander à Doctrine de persister
        $this->em->persist($data);
        $this->em->flush();
    }

    public function remove($data)
    {
        // 1. Demander à doctrine de supprimer l'utilisateur
        $this->em->remove($data);
        $this->em->flush();
    }
}