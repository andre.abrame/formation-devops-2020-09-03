<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

define("DEFAULT_PASSWORD", "123456");
define("ROLE", "ROLE_USER");
define("ONLINE", "online");

class AppFixtures extends Fixture
{
	 private $encoder;
    
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        

        /* User fixtures */

        $user1 = new User();
        $user1->setFirstName("john");
        $user1->setLastName("doe");
        $user1->setEmail("john.doe@gmail.com");
        $user1->setPassword($this->encoder->encodePassword($user1, DEFAULT_PASSWORD));
        $user1->setRoles(array(ROLE));
        $user1->setStatus(ONLINE);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setFirstName("jane");
        $user2->setLastName("doe");
        $user2->setEmail("jane.doe@gmail.com");
        $user2->setPassword($this->encoder->encodePassword($user2, DEFAULT_PASSWORD));
        $user2->setRoles(array(ROLE));
        $user2->setStatus(ONLINE);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setFirstName("jean");
        $user3->setLastName("dupond");
        $user3->setEmail("jean.dupand@gmail.com");
        $user3->setPassword($this->encoder->encodePassword($user3, DEFAULT_PASSWORD));
        $user3->setRoles(array(ROLE));
        $user3->setStatus("busy");
        $manager->persist($user3);

        $user4 = new User();
        $user4->setFirstName("jean");
        $user4->setLastName("dupond4");
        $user4->setEmail("jean.dupand4@gmail.com");
        $user4->setPassword($this->encoder->encodePassword($user4, DEFAULT_PASSWORD));
        $user4->setRoles(array(ROLE));
        $user4->setStatus("offline");
        $manager->persist($user4);

        $user5 = new User();
        $user5->setFirstName("jeanne");
        $user5->setLastName("dupong");
        $user5->setEmail("jeanne.dupong@gmail.com");
        $user5->setPassword($this->encoder->encodePassword($user5, DEFAULT_PASSWORD));
        $user5->setRoles(array(ROLE));
        $user5->setStatus("offline");
        $manager->persist($user5);

        $user6 = new User();
        $user6->setFirstName("joe");
        $user6->setLastName("doe");
        $user6->setEmail("joe.doe@gmail.com");
        $user6->setPassword($this->encoder->encodePassword($user6, DEFAULT_PASSWORD));
        $user6->setRoles(array(ROLE));
        $user6->setStatus(ONLINE);
        $manager->persist($user6);

        $manager->flush();
    }
}
